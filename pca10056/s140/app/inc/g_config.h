#ifndef _G_CONFIG_H
#define _G_CONFIG_H
#include <stdio.h>
#include <stdint.h>
#include "nrf_log.h"
/*
__LINE__：在源代码中插入当前源代码行号；

__FILE__：在源文件中插入当前源文件名；

__DATE__：在源文件中插入当前的编译日期

__TIME__：在源文件中插入当前编译时间；

__STDC__：当要求程序严格遵循ANSI C标准时该标识被赋值为1；

__cplusplus：当编写C++程序时该标识符被定义。
*/

#define LOG_DEBUG
#ifdef LOG_DEBUG
    #define  log(...)  NRF_LOG_INTERNAL_INFO( __VA_ARGS__)
    //#define log(format,...) NRF_LOG_INTERNAL_INFO("FILE: "__FILE__", LINE: %d: "format"\r\n", __LINE__, ##__VA_ARGS__)
#else
    #define log(...)
#endif


#define PRINTF_DEBUG

#ifdef PRINTF_DEBUG
    //#define print(format, ...) printf (format, ##__VA_ARGS__)
    #define print(format,...) printf("FILE: "__FILE__", LINE: %d: "format"\r\n", __LINE__, ##__VA_ARGS__)
#else
    #define print(format, ...)
#endif



#endif
