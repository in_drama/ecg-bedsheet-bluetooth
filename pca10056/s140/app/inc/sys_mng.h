#ifndef _SYS_MNG_H
#define _SYS_MNG_H
#include "stdint.h"
#include "g_config.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

//定时器周期：100ms
#define TIMER_PERIOD_1000MS      pdMS_TO_TICKS(1000)

/* 任务句柄 */
extern TaskHandle_t sys_task_thread;
/* 定时器句柄 */
extern TimerHandle_t timer_1hz;

#pragma pack(1)
typedef struct
{
    uint8_t         u8Event;
    uint8_t         u8Parameter;
    uint8_t*        pdata; 
}SYS_MSG;

enum
{
    MSG_SYS_START = 1,
    MSG_1HZ,
    MSG_NUS_SEND,
};
#pragma pack()
extern SYS_MSG msg;;

void task_Message_Send(SYS_MSG msg);
void task_Message_SendFromISR(SYS_MSG msg);
void tasks_init(void);
void timer_init(void);

#endif
