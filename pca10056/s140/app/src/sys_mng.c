#include "sys_mng.h"
#include "stdio.h"
#include "main.h"

extern uint32_t ble_data_send(uint8_t* p_data, uint16_t p_length);

SYS_MSG msg = {0};

static QueueHandle_t sys_msg = NULL;
/* 任务句柄 */
TaskHandle_t sys_task_thread;
/* 定时器句柄 */
TimerHandle_t timer_1hz;

void vApplicationIdleHook( void )
{
    //log("vApplicationIdleHook.");
}

void vApplicationTickHook(void)
{
    //log("vApplicationTickHook.");
}

void vApplicationMallocFailedHook(void)
{
    //log("vApplicationMallocFailedHook.");
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
    //log("vApplicationStackOverflowHook: %s.", pcTaskName);
}

void task_Message_Send(SYS_MSG msg)
{
    //发出消息
    if (NULL != sys_msg)
    {
        xQueueSend(sys_msg, &msg, 0);
    }
}

void task_Message_SendFromISR(SYS_MSG msg)
{
    //发出消息
    if (NULL != sys_msg)
    {
        xQueueSendFromISR(sys_msg, &msg, 0);
    }
}


void sys_task_handler(void *arg)
{
    UNUSED_PARAMETER(arg);
	SYS_MSG msg = {0};
    sys_msg = xQueueCreate(10, sizeof(msg));
    
     //启动系统事件
    msg.u8Event = MSG_SYS_START;
    task_Message_Send(msg);
    while (1)
    {
        if (pdPASS == xQueueReceive(sys_msg, &msg, 100/portTICK_PERIOD_MS))
        {
            switch (msg.u8Event)
            {
                case MSG_SYS_START:
                {
                    advertising_start();
                }
                break;
                case MSG_1HZ:
                {
                    NRF_LOG_INFO("1HZ\r\n");
                }
                break;
                case MSG_NUS_SEND:
                {
                    ble_data_send(msg.pdata,msg.u8Parameter);
                }
                break;
                default:
                break;
            }
        }
    }
}

void timer_1hz_handler(TimerHandle_t xTimer)
{
    UNUSED_PARAMETER(xTimer);
    msg.u8Event = MSG_1HZ;
    task_Message_Send(msg);
}

void tasks_init(void)
{
    // 训练任务.
    if (pdPASS != xTaskCreate(sys_task_handler, "sys_mng", (4*1024), NULL, 1, &sys_task_thread))
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }
    
     // Start FreeRTOS scheduler.
    vTaskStartScheduler();
}

void timer_init(void)
{
     timer_1hz = xTimerCreate("TIMER_1HZ", TIMER_PERIOD_1000MS, pdTRUE, NULL, timer_1hz_handler);

    /* Error checking */
    if ((NULL == timer_1hz) )//|| (NULL == timer_10ms))
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
        return;
    }
    
    // Start application timers.
    if (pdPASS != xTimerStart(timer_1hz, 0))
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }
}
